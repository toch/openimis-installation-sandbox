FROM ubuntu:jammy

ARG SSH_PASS=ubuntu
RUN apt-get -y update && \
    apt-get -y --no-install-recommends install --fix-missing sudo openssh-server && \
    rm -rf /var/lib/apt/lists/*
RUN useradd -m -s /bin/bash -g root -G sudo openimis
RUN addgroup openimis && adduser openimis openimis
RUN echo "openimis:${SSH_PASS}" | chpasswd
COPY files/sshd_config /etc/ssh/sshd_config
COPY files/start.sh /usr/local/bin/start.sh
RUN chmod 755 /usr/local/bin/start.sh
EXPOSE 22
CMD ["/usr/local/bin/start.sh"]