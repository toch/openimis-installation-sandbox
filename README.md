# OpenIMIS, installation sandbox

This sandbox allows to reproduce an installation on a Ubuntu VM (e.g. AWS EC2)
with a given Microsfot SQL Server database.

It follows the [OpenIMIS guideline](https://docs.google.com/document/d/193kX82ufWMkzUiTiOxb9IXDP1PwZnHfXLQ8pHgQDdW8/edit#).

## Requirements

* Ubuntu 22.04
* Docker 24.02
* SSH Agent

## Configuration

Copy the `.env.example` file and set the variables:

| Variable | Value | Description |
| - | - | - |
| `HOST_SSH_PORT` | integer | This will be the port you can use on your localhost to access the container by SSH |
| `PUBLIC_KEY` | text | This is the public key corresponding to the SSH identity key that you'll use to authenticate you with the SSH server (see next section). |
| `DB_MASTER_USERNAME` | text | This is the username of the super admin to use to connect to the Microsoft SQL Server|
| `DB_MASTER_PASSWORD` | text | The password of the super admin. The password must be at least 8 characters long and contain characters from three of the following four sets: Uppercase letters, Lowercase letters, Base 10 digits, and Symbols.|
| `DB_USERNAME` | text | The username to connect to the OpenIMIS database. |
| `DB_PASSWORD` | text | The password to connect to the OpenIMIS database (it follows the same rule than the Master password). |
| `OPENIMIS_MODULES` | text | The list of backend modules to install. Check the `.env.example` for a common one. |
| `OPENIMIS_FE_MODULES` | text | The list of frontend modules to install Check the `.env.example` for a common one. |
| `OPENIMIS_URL` | text | The exposed URL of the web application. |

## Generate fake SSH keys

Run the following to generate SSH keys to connect to the VM:

```bash
ssh-keygen -t rsa -C "Fake SSH Keys for OpenIMIS installation sandbox" -f keyfiles/id_rsa -N ""
```

Use the content of `keyfiles/id_rsa.pub` to set the envioronment variable
`PUBLIC_KEY`.

## Default credential

* username: `openimis`
* password: `ubuntu`

## Usage

Manually you can spawn the server and connect to it as it follows:

```bash
docker compose up -d vm
ssh -A -i <path-to-your-rsa-key> -p <HOST_SSH_PORT> openimis@localhost
```

We provide an installation script `install.sh`. Its usage is the following:
```
Usage: ./install.sh [-h] [-n] [-f STEP]
       This simulates the installation of OpenIMIS on a remote Ubuntu
       server (e.g. running on AWS) but on a local Docker container.

  -h         shows usage
  -r         resets running VM
  -b         forces building of VM
  -f STEP    executes scripts starting from STEP

```

If you want to test the installation, type the following:

```bash
./install.sh
```

## Installation playbooks, differences, and known limitations

The installation playbooks are in the directory `scripts`. They are executed
in ascending order of their prefix. Each of them correspond to an important
step of the installation guideline.

Some commands require super user rights. As it is automated, you might find
at the top of the playbook the following command to initiate the `sudo` session.

```bash
sudo --validate -S <<<"$(cat /run/secrets/vm_sudo_password)" &>/dev/null
```

This works only in the present sandbox as we are using Docker and one scret
mount as `vm_sudo_password`.

As such, this is not part of the guideline.

There are some differences with the initial guide, either to automate the
configuration or to fix some issues, mainly missing dependencies that could
be due to the difference with the image used by AWS EC2.

## TODO

* [ ] install C# REST API
* [ ] config the URL
* [ ] local SSL certs