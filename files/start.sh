#!/bin/bash

mkdir -p /home/openimis/.ssh/
chmod 700 /home/openimis/.ssh/
chown openimis:root /home/openimis/.ssh

if [[ ! -f /home/openimis/.ssh/authorized_keys ]]; then
  touch /home/openimis/.ssh/authorized_keys
  chmod 600 /home/openimis/.ssh/authorized_keys
  chown openimis:root /home/openimis/.ssh/authorized_keys
fi

if [[ -n $PUBLIC_KEY ]]; then
  if ! grep -q "${PUBLIC_KEY}" /home/openimis/.ssh/authorized_keys; then
    echo "$PUBLIC_KEY" >>/home/openimis/.ssh/authorized_keys
    echo "Public key from env variable added"
  fi
fi

sed -i -e "/^DB_.*/d" -e "/^OPENIMIS_.*/d" /etc/environment
env | grep DB >>/etc/environment
env | grep OPENIMIS >>/etc/environment

service ssh start -D
