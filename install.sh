#!/bin/bash

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

# Constants
USERNAME=openimis
SSH_KEY="${SCRIPT_DIR}/keyfiles/id_rsa"

# Options
OPTSTRING=":hrbf:"
OPT_N_BUILD=0
OPT_N_RESET=0
OPT_N_FROM_STEP=1

function usage() {
  echo "Usage: $0 [-h] [-n] [-f STEP]" 1>&2
  echo "       This simulates the installation of OpenIMIS on a remote Ubuntu" 1>&2
  echo "       server (e.g. running on AWS) but on a local Docker container." 1>&2
  echo 1>&2
  echo "  -h         shows usage" 1>&2
  echo "  -r         resets running VM" 1>&2
  echo "  -b         forces building of VM" 1>&2
  echo "  -f STEP    executes scripts starting from STEP" 1>&2
  exit 1
}

function load_dotenv() {
  if [[ -f "${SCRIPT_DIR}/.env" ]]; then
    set -a
    source "${SCRIPT_DIR}/.env"
    set +a
  else
    echo "Please provide a \`.env\`. Copy the example \`.env.example\` and set the variables."
    exit 1
  fi
}

function wait_for() {
  local try=0
  local success=0
  local port=$1
  echo -n "Waiting for SSH to be up on ${port}:"
  while
    if nc -z localhost "${port}" 2>/dev/null; then
      success=1
    else
      ((try++))
    fi
    [[ $success -eq 0 ]] && [[ try -lt 5 ]]
  do
    echo -n " ${try}"
    sleep 5
  done
  if [[ $success -eq 0 ]]; then
    echo
    echo "The port ${port} is not opened on localhost."
    echo "Please check that the SSH server is running with a \`docker compose ps\`."
    echo "Please check thet ${port} is not used by another process or"
    echo "blocked by networking rules."
    exit 1
  else
    echo " UP!"
  fi
}

function reset() {
  local do_we_reset=$1
  [[ $do_we_reset -ne 1 ]] && return

  echo "Cleaning and stopping running VM"
  docker compose down
}

function spawn() {
  local do_we_build=$1
  echo -n "Spawning a VM"
  if ((do_we_build == 1)); then
    echo " (building forced)"
    docker compose up --build -d vm
  else
    echo
    docker compose up -d vm
  fi
}

function set_up_ssh() {
  echo "Setting up SSH keys and fingerprint"
  ssh-keygen -R "[localhost]:${HOST_SSH_PORT}" &>/dev/null
  ssh-keygen -R "127.0.0.1:${HOST_SSH_PORT}" &>/dev/null
  sleep 1
  ssh-keyscan -T 10 -H -t rsa -p "${HOST_SSH_PORT}" localhost >>~/.ssh/known_hosts 2>/dev/null
}

while getopts "${OPTSTRING}" opt; do
  case "${opt}" in
  r)
    OPT_N_RESET=1
    ;;
  b)
    OPT_N_BUILD=1
    ;;
  f)
    OPT_N_FROM_STEP=${OPTARG}
    ;;
  *)
    usage
    ;;
  esac
done
shift $((OPTIND - 1))

load_dotenv

SSH_COMMAND="ssh -A -i ${SSH_KEY} -p ${HOST_SSH_PORT} ${USERNAME}@localhost"

reset $OPT_N_RESET
spawn $OPT_N_BUILD

wait_for "${HOST_SSH_PORT}"

set_up_ssh

for script in "${SCRIPT_DIR}/scripts/"*.sh; do
  step=$((10#$(basename "${script}" | sed -e 's/^\([[:digit:]]\+\)_.*/\1/')))
  ((step >= OPT_N_FROM_STEP)) && ${SSH_COMMAND} 'BASH_ENV=~/.profile bash -s' <"${script}"
done
