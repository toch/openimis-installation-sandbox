#!/bin/bash

sudo --validate -S <<<"$(cat /run/secrets/vm_sudo_password)" &>/dev/null

echo "5.1 Preparing the Ubuntu EC2 server: install required packages"

sudo apt-get update
sudo apt-get upgrade -y
# DIFF: Added jq systemctl
sudo apt-get install -y --no-install-recommends openssh-server curl git python3-venv \
  python3-wheel libpq-dev python3-dev gcc g++ make docker-compose nginx gnupg jq \
  systemctl

echo "5.1 Preparing the Ubuntu EC2 server: install database drivers"
sudo su
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/prod.list >/etc/apt/sources.list.d/mssql-release.list
exit
sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql17
sudo ACCEPT_EULA=Y apt-get install -y mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >>~/.profile
source ~/.profile
sudo apt-get install -y unixodbc-dev
