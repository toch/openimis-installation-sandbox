#!/bin/bash

(
  cd ~/openimis-one-gambia
  git restore dump.sql
  cp -f dump.sql dump.sql.bck
  iconv -f utf-16 -t utf-8 dump.sql.bck | sed -e 's/D:\\rdsdbdata\\DATA\\/\/var\/opt\/mssql\/data\//g' -e "/\[admin\]/d" -e "/\[imis\]/d" | iconv -f utf-8 -t utf-16 >dump.sql
)

echo "5.2.2 - Configure the database"

cd ~/openimis-one-gambia
sqlcmd -S "${DB_HOST}" -U "${DB_MASTER_USERNAME}" -P "${DB_MASTER_PASSWORD}" -Q "CREATE LOGIN ${DB_USERNAME} WITH PASSWORD = '${DB_PASSWORD}'"
sqlcmd -S "${DB_HOST}" -U "${DB_MASTER_USERNAME}" -P "${DB_MASTER_PASSWORD}" -i dump.sql | grep . | uniq -c

# # use admin and db openimis from here !
sqlcmd -d openimis -S "${DB_HOST}" -U "${DB_MASTER_USERNAME}" -P "${DB_MASTER_PASSWORD}" -Q "CREATE USER ${DB_USERNAME} FOR LOGIN ${DB_USERNAME}"
sqlcmd -d openimis -S "${DB_HOST}" -U "${DB_MASTER_USERNAME}" -P "${DB_MASTER_PASSWORD}" -Q "EXEC sp_addrolemember N'db_owner', N'${DB_USERNAME}'"
