#!/bin/bash

sudo --validate -S <<<"$(cat /run/secrets/vm_sudo_password)" &>/dev/null

echo "5.2.3 - Configure the backend"
cd openimis-one-gambia/
source venv/bin/activate
echo "5.2.3 - Configure the backend: set the module versions"
(
  cd openimis-be_py
  git restore openimis.json
  cp -f openimis.json openimis.json.bck
  jq ".modules = [.modules[] | select(.name | test(\"^$(echo "${OPENIMIS_MODULES}" | sed -e "s/\",\"/\$|\^/g")$\"))]" openimis.json.bck | sed -e "s/develop/release\/23.04/" >openimis.json

  echo "5.2.3 - Configure the backend: install the modules"
  echo "tzdata" >>requirements.txt
  pip install -r requirements.txt
)
(
  cd openimis-be-claim_batch_py
  sed -i -e "s/pandas==1.1.4/pandas~=1.4.2/" setup.py
  pip install -e .
)
for module in openimis-be-*; do
  echo "${module}"
  pip install -e "${module}"
done

echo "5.2.3 - Configure the backend: generate the configuration file"

cd openimis-be_py
cat <<EOF >.env
DB_HOST=${DB_HOST}
DB_PORT=1433
DB_NAME=openimis
DB_USER=${DB_USERNAME}
DB_PASSWORD=${DB_PASSWORD}
DB_ENGINE=mssql
ACCEPT_EULA=Y
INSUREE_NUMBER_LENGTH=12
INSUREE_NUMBER_MODULE_ROOT=10
SITE_ROOT=api
MASTER_PASS=')(#$1HsD'

PHOTO_ROOT_PATH=/home/${USER}/openimis-one-gambia/pictures
EOF

echo "5.2.3 - Configure the backend: generate the static files"

cd openIMIS
./manage.py collectstatic

echo "5.2.3 - Configure the backend: run migrations"
./manage.py migrate

# maybe check it works

echo "5.2.3 - Configure the backend: configure the automated restart"

cat <<EOF >/tmp/openimis.service
[Unit]
Description=openIMIS backend
After=network-online.target

[Service]
WorkingDirectory=/home/${USER}/openimis-one-gambia/openimis-be_py/openIMIS/
ExecStart=/home/${USER}/openimis-one-gambia/venv/bin/python manage.py runserver
Restart=always
RestartSec=15s
KillMode=process
TimeoutSec=infinity
User=${USER}
Group=${USER}

[Install]
WantedBy=multi-user.target
EOF

sudo chown root:root /tmp/openimis.service
sudo mv /tmp/openimis.service /lib/systemd/system/openimis.service

# config
sudo systemctl start openimis.service
sudo systemctl enable openimis.service
