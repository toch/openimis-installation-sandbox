#!/bin/bash

sudo --validate -S <<<"$(cat /run/secrets/vm_sudo_password)" &>/dev/null

echo "5.2.4 - Configure the frontend"

echo "5.2.4 - Configure the frontend: install Node.js"

curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "5.2.4 - Configure the frontend: install yarn"

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

echo "5.2.4 - Configure the frontend: set modules versions"
cd openimis-one-gambia/openimis-fe_js

OPENIMIS_FE_MODULES='CoreModule","HomeModule","LocationModule","InsureeModule","MedicalModule","MedicalPriceListModule","ProductModule","PolicyModule","PayerModule","ContributionModule","PaymentModule","ClaimModule","ClaimBatchModule","AdminModule","ToolsModule","ProfileModule","LanguageEnGmbModule","CalculationModule","PolicyHolderModule","ContributionPlanModule","InvoiceModule'

git restore openimis.json
cp -f openimis.json openimis.json.bck
sed -e "s/LanguageFr/LanguageEnGmb/" -e "s/language_fr/language_en_gmb/" openimis.json.bck | jq ".modules = [.modules[] | select(.name | test(\"^$(echo "${OPENIMIS_FE_MODULES}" | sed -e "s/\",\"/\$|\^/g")$\"))]" | sed -e "s/develop/release\/23.04/" >openimis.json

echo "5.2.4 - Configure the frontend: install dependencies"
export GENERATE_SOURCEMAP=false
yarn load-config
yarn install

echo "5.2.4 - Configure the frontend: install modules"
(
  cd ..
  ./fe_link.sh
)

echo "5.2.4 - Configure the frontend: build"
yarn install
yarn build
yarn global add serve
sudo mkdir /var/www/html/front/
sudo chown "${USER}" /var/www/html/front/
cp -r build/* /var/www/html/front/
