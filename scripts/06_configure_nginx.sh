#!/bin/bash

sudo --validate -S <<<"$(cat /run/secrets/vm_sudo_password)" &>/dev/null

echo "5.2.4 - Configure NGINX"

sudo rm /etc/nginx/sites-enabled/default # remove the default site (it’s just a link, don’t worry)
cat <<EOF >/tmp/nginx-openimis.conf
upstream docker-backend {
      server localhost:8000;
}
upstream docker-frontend {
      server localhost:3000;
}
upstream restapi {
      server localhost:8080;
}
server {
      server_name ${OPENIMIS_URL};
#     return 301 https://$host$request_uri;
#}
#server {
#
#     listen    443 ssl;
#     server_name ${OPENIMIS_URL};
#
##      ssl_certificate /etc/ssl/certs/example.openimis.org.crt;
##      ssl_certificate_key /etc/ssl/private/example.openimis.org.key;

      client_max_body_size 100M;

      location /.well-known {
              root /var/www/html;
      }

      location /LegacyHome {
              return 204;
      }

      location /keepLegacyAlive {
              return 204;
      }

      location / {
              return 301 /front/;
      }
     
      location /home {
              return 301 /front/;
      }

      location /Home.aspx {
              return 301 /front/;
      }

      location ~/front/(.*) {
              root /var/www/html;
              try_files $uri $uri/ /front/index.html;
              #error_page 404 $scheme://$host/front/;
      }

      location /iapi/ {
              proxy_pass http://docker-backend/api/;
              proxy_set_header   Host $host;
              proxy_set_header   X-Real-IP $remote_addr;
              proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header   X-Forwarded-Host $server_name;
      }

      location /api/ {
              proxy_pass http://docker-backend/api/;
              proxy_set_header   Host $host;
              proxy_set_header   X-Real-IP $remote_addr;
              proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header   X-Forwarded-Host $server_name;
      }

      location /rest/ {
              proxy_pass http://restapi/;
              proxy_set_header   Host $host;
              proxy_set_header   X-Real-IP $remote_addr;
              proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
     proxy_set_header   X-Forwarded-Proto https;
              proxy_set_header   X-Forwarded-Host $server_name;
      }
}
EOF
sudo ln -s /etc/nginx/sites-available/openimis.conf /etc/nginx/sites-enabled/openimis.conf

sudo systemctl restart nginx.service
echo 'tzdata tzdata/Areas select Europe' | sudo debconf-set-selections
echo 'tzdata tzdata/Zones/Europe select Brussels' | sudo debconf-set-selections
DEBIAN_FRONTEND="noninteractive" sudo apt-get install -y python3-certbot-nginx
# sudo certbot --nginx -d ${OPENIMIS_URL}
